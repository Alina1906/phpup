<?php

namespace common\models;

/**

example
$resizer = new ImageResize($this->picture->tempName);
$resizer->resize($width, $height, true);


 * Class ImageResize
 * @package application\models
 */
class ImageResize
{
    /** @var integer исходная ширина */
    private $sourceWidth;

    /** @var integer исходная высота */
    private $sourceHeight;

    /** @var string Путь к картинке */
    private $path;

    /** @var string mime тип картинки */
    private $mimeType;

    /** Ключи результирующего массива метода getimagesize*/
    const WIDTH_SIZE_IMAGE = 0;
    const HEIGHT_SIZE_IMAGE = 1;
    const MIME_TYPE_IMAGE = 'mime';

    /**
     * ImageResize constructor.
     *
     * @param string $path Путь  к исходному изображению
     */
    public function __construct( $path)
    {
        $imageInfo = getimagesize($path);
        $this->path = $path;
        $this->sourceWidth = $imageInfo[self::WIDTH_SIZE_IMAGE];
        $this->sourceHeight = $imageInfo[self::HEIGHT_SIZE_IMAGE];
        $this->mimeType = $imageInfo[self::MIME_TYPE_IMAGE];
    }

    /**
     * Изменение размера картинки
     *
     * @param int $width Необходимая ширина файла
     * @param int $height Необходимая высота файла
     * @param bool $keepProportion Сохранение пропрорций исходного изображения
     */
    public function resize( $width,  $height,  $keepProportion = false)
    {
        $finalWidth = $width;
        $finalHeight = $height;
        if ($keepProportion) {
            $sourceRelation = $this->sourceWidth / $this->sourceHeight;
            if ($width / $height > $sourceRelation) {
                $finalWidth = $height * $sourceRelation;
            } else {
                $finalHeight = $width / $sourceRelation;
            }
        }

        if ($this->mimeType == 'image/png') {
            $this->resizeImagePng($finalWidth, $finalHeight);
        }

        if (in_array($this->mimeType, ['image/jpg', 'image/jpeg'])) {
            $this->resizeImageJpg($finalWidth, $finalHeight);
        }
    }

    /**
     *  Изменение размера картинки в формате png*
     *
     * @param int $width Необходимая ширина файла
     * @param int $height Необходимая высота файла
     */
    private function resizeImagePng(int $width, int $height): void
    {
        $src = imagecreatefrompng($this->path);
        $dst = imagecreatetruecolor($width, $height);

        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $this->sourceWidth, $this->sourceHeight);
        imagepng($dst, $this->path, 5);
    }

    /**
     * Изменение размера картинки jpg
     *
     * @param int $width Необходимая ширина файла
     * @param int $height Необходимая высота файла
     */
    private function resizeImageJpg(int $width, int $height): void
    {
        $src = imagecreatefromjpeg($this->path);
        $dst = imagecreatetruecolor($width, $height);

        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $this->sourceWidth, $this->sourceHeight);
        imagejpeg($dst, $this->path);
    }
}