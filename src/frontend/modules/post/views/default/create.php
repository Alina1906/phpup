<?php
/* @var $this yii\web\View */
/* @var $model frontend\modules\post\models\forms\PostForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 ">
            <form>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Выберите фото</label>
                    <?php $form = ActiveForm::begin(); ?>
                    <?php echo $form->field($model, 'picture')->fileInput(); ?>
                </div>
<!--                <button type="button" class="btn btn-outline-primary">Загрузить</button>-->
                <?php echo Html::submitButton('Create'); ?>
                <?php ActiveForm::end(); ?>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<div class="post-default-index">
