<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\FontAwesomeAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 image-header-block center-block">
                <a href="<?php echo Url::to(['/site/index']); ?>">
                    <img src="/img/head.png" class="image-header ">
                </a>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    <nav class="main-menu">
        <?php
        $menuItems[]= '<li>'. Html::beginForm(['/site/index'], 'post') . Html::submitButton(
                'Главная', ['class' => 'btn btn-link home']) . Html::endForm() . '</li>';
        if (Yii::$app->user->isGuest) {
            $menuItems[] = '<li>'. Html::beginForm(['/user/default/signup'], 'post') . Html::submitButton(
                    'Регистрация', ['class' => 'btn btn-link sign-up']) . Html::endForm() . '</li>';
            $menuItems[] = '<li>'. Html::beginForm(['/user/default/login'], 'post') . Html::submitButton(
                    'Войти', ['class' => 'btn btn-link login']) . Html::endForm() . '</li>';
        } else {
            $menuItems[] = '<li>'. Html::beginForm([Url::to(['/user/profile/view', 'nickname' => Yii::$app->user->identity->getNickname()])], 'post') . Html::submitButton(
                    'Профиль', ['class' => 'btn btn-link profile']) . Html::endForm() . '</li>';
            $menuItems[] = '<li>'. Html::beginForm(['/post/default/create'], 'post') . Html::submitButton(
                    'Добавить', ['class' => 'btn btn-link add']) . Html::endForm() . '</li>';
            $menuItems[] = '<li>'
                . Html::beginForm(['/user/default/logout'], 'post')
                . Html::submitButton(
                    'Выйти',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'nav nav-pills center-block navbar-nav navbar-right', 'style' => 'margin: 0 auto;'],
            'items' => $menuItems,
        ]);
        ?>
    </nav>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    <footer>
    </footer>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


    <?php
//    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);

//    NavBar::end();
    ?>




