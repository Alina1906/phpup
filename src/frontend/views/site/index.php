<?php
/* @var $this yii\web\View */
/* @var $currentUser frontend\models\User */

/* @var $feedItems [] frontend\models\Feed */

use yii\web\JqueryAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'Новости';
?>
    <div class="page-posts no-padding">
    <div class="row">
    <div class="page page-post col-sm-12 col-xs-12">
        <div class="blog-posts blog-posts-large">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 center-block" style="margin: 0px auto;">
                    <?php if ($feedItems): ?>
                        <?php foreach ($feedItems as $feedItem): ?>
                            <?php /* @var $feedItem Feed */ ?>
                            <div class="block-post  row center-block">
                                <div class="col-md-12 center-block">
                                    <div class="thumbnail">
                                        <div class="media" style="float: left; padding-bottom: 10px;">
                                            <div style="display: inline-block">
                                                <img src="<?php echo $feedItem->author_picture; ?>"
                                                     class="mr-3 img-circle" alt="user"
                                                     style="width: 50px;">
                                                <span class="mt-0 mb-1">
                                                    <a href="<?php echo Url::to(['/user/profile/view', 'nickname' => ($feedItem->author_nickname) ? $feedItem->author_nickname : $feedItem->author_id]); ?>">
                                                    <?php echo Html::encode($feedItem->author_name); ?>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                        <a href="<?php echo Url::to(['/post/default/view', 'id' => $feedItem->post_id]); ?>">
                                        <img src="<?php echo Yii::$app->storage->getFile($feedItem->post_filename); ?>" class="card-img-top"
                                             alt=" " style="width: 100%;">
                                        <div class="caption">
                                            <ul class="list-unstyled" style="padding-bottom: 0px;margin-bottom: 5px;">
                                                <li class="media">
                                                        <p class="post-likes">
                                                            <a href="#"
                                                               class="button-unlike" <?php echo ($currentUser->likesPost($feedItem->post_id)) ? "" : "display-none"; ?>"
                                                                data-id="<?php echo $feedItem->post_id; ?>">
                                                                &nbsp;&nbsp;<span class="glyphicon glyphicon-heart"></span>
                                                            </a>
                                                            <a href="#"
                                                               class="button-like" <?php echo ($currentUser->likesPost($feedItem->post_id)) ? "display-none" : ""; ?>"
                                                                data-id="<?php echo $feedItem->post_id; ?>">
                                                                &nbsp;&nbsp;<span class="glyphicon glyphicon-heart"></span>
                                                            </a>
                                                            <span class="likes-count"><?php echo $feedItem->countLikes(); ?></span>
                                                        </p>
                                                        <p class="post-date" style="text-align: right;">
                                                            <span><?php echo Yii::$app->formatter->asDatetime($feedItem->post_created_at); ?></span>
                                                        </p>
                                                    <div class="media-body"
                                                         style="padding-left: 15px; padding-top: 7px;">
                                                        <p class="card-text"><?php echo HtmlPurifier::process($feedItem->post_description); ?>
                                                        </p>
                                                    </div>
                                                    <div > comments</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                        <div class="col-md-12">
                            Nobody posted yet!
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJsFile('@web/js/likes.js', [
    'depends' => JqueryAsset::className(),
]);
$this->registerJsFile('@web/js/complaints.js', [
    'depends' => JqueryAsset::className(),
]);
